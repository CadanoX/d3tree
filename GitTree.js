//const d3 = require('d3');
{
	class GitTree {
		constructor(container, eventHandler) {
			this._container = container;
			this._eventHandler = eventHandler;
			this._data = {};
			this._treeData; // maybe use to optimize speed
			this.tree = d3.myTree(container).parentId("pId");
			//this.equalityFunction = equalAst;
		}

		data(d) { return d == null ? this._data : (this.add(d), this); }

		add(d) {
			if (!d || (typeof d !== "object")) return console.log(`ERROR: Added data "${d}" is not an object.`);
			
			if (!!d.id)
				this._addObject(d);
			else if (Array.isArray(d))
				d.forEach(n => this._addObject(n));
			else // object of objects
				for (n in d) this._addObject(n)

			this._update();
		}

		_addObject(d) {
			if (d.pId) {
				const parent = this._data[d.pId];
				if (typeof parent === 'undefined') return console.log(`ERROR: Parent "${d.pId}" of node "${d.id}" is not in the current data.`);
				if (d.branches.length > 1) return console.log("ERROR: Added node has more than 1 branch. This is not handled.");
				
				this._addBranchInformation(parent, d.branches[0]);
				this._addMergeInformation(d, parent);
			}
			this._data[d.id] = d;
		}

		_addBranchInformation(parent, branch) {
			if (!parent.branches.includes(branch))
				do { parent.branches.push(branch) }
				while (parent = this._data[parent.pId])
		}

		_addMergeInformation(d, p) {
			/*
			if (equalityFunction(d, p)) {
				if (!!p.mergeId) // inherit mergeId
					d.mergeId = p.mergeId;
				else //or merge with parent
					d.mergeId = p.id;
				
				this._data[d.mergeId].numMerged++;
			}
			else {
				d.mergeId = null;
				this._emit({ name: 'newMergeNode', data: { ast: node.ast, id: node.id } });
			}
			d.merged = false;
			*/
		}

		/*
		 * Copy and modify the raw data, to only represent the rendered data.
		*/
		_update() {
			this._treeData = d3.values(this._data);
			this.tree.nodes(this._treeData).render();
		}

		_emit(data) { this.eventHandler.emit('gitgraph', data); }
	}

	

	d3.GitTree = (...args) => new GitTree(...args);
}