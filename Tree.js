//const d3 = require('d3');
{
	class Tree {
		constructor(container,
			opts = {
				animDuration: 1000,
				margin: { top: 20, right: 20, bottom: 20, left: 20 },
				nodeSpacing: { x: 100, y: 150, maxImgX: 99, separation: 1.25 },
				nodeRadius: 20
			}) {
			this._container = container;
			this._nodes;
            this._links;
            this._parentId = "parentId";
            this._id = "id";
            this._opts = opts;
            this._layout = this._myTreeLayout;

			this._init();
		}

		nodes(n) { return n == null ? this._nodes : (this._nodes = n, this._nodesChanged(), this); }
		links(n) { return n == null ? this._links : (this._links = n, this); }
        id(id) { return id == null ? this._id : (this._id = id, this); }
        parentId(id) { return id == null ? this._parentId : (this._parentId = id, this); }

		_init() {
			const { margin, nodeSpacing } = this._opts;
			this._svg = d3.select(this._container)
				.append('svg')
				.classed('gitgraph', 'true')
				.attr('height', this._container.clientHeight)
				.attr('width', this._container.clientWidth)
				//.on("contextmenu", () => d3.event.preventDefault())
				.append('g')
				.attr('id', 'svg-drawn')
				.attr('transform', "translate(" + margin.left + "," + margin.top + ")");

			this._linkContainer = this._svg.append('g').classed('linkContainer', true);
			this._nodeContainer = this._svg.append('g').classed('nodeContainer', true);
        }

        _nodesChanged() {
            const data = d3.stratify().id(d => d[this._id]).parentId(d => d[this._parentId])( this._nodes );
            const treeData = this._layout(data);
			this._nodes = treeData.descendants();
			this._links = treeData.links();
        }
        
        _myTreeLayout(d, pos = 0) {
            if (!d.parent) { // root node;
                d.x = 0;
                d.y = 0;
            }
            else {
				let {nodeSpacing} = this._opts;
                d.x = d.parent.x - pos * nodeSpacing.x;
                // increase spacing, if the node has more merged children, than images fit into the imageCollection
                let spacingFactor = 1; 
                if ((d.parent.data.numMerged > 0) && d.userDemerged)
                    Math.ceil((d.parent.data.numMerged + 1) / nodeSpacing.maxImgX);
                d.y = d.parent.y + nodeSpacing.y * spacingFactor;
            }
            if (d.children)
                d.children.forEach((child,i) => this._myTreeLayout(child, i));
            
            return d;
		}
		
		_diagonal(s, t) {
			return `M ${s.x} ${s.y}
					C ${(s.x + t.x) / 2} ${s.y},
					  ${(s.x + t.x) / 2} ${t.y},
					  ${t.x} ${t.y}`;
		}

		render() {
			const { animDuration } = this._opts;

			this._renderNodes();
			this._renderLinks();

			// TODO: precompute resize size, to resize before the animation
			this._reposition()
			setTimeout(() => this._reposition(), animDuration);

			return this;
		}

		_renderNodes() {
			const { animDuration, nodeRadius } = this._opts;
			let nodes = this._nodeContainer.selectAll('g.node')
				.data(this._nodes, (d, i) => d.id);

			if (nodes.length == 0) return this;

			nodes.enter().each((d) => {
				let p = d;
				while (!!(p = p.parent)) {
					if (!nodes.filter(d => p.id === d.id).empty()) {
						d.vParent = p;
						break;
					}
				}
			})
			// Enter any new nodes at the parent's previous position.
			let nodesEnter = nodes.enter().append('g')
				.attr('class', d => "node commit-" + d.id)
				.attr('transform', d => {
						!!d.vParent ? "translate(" + d.vParent.x + "," + d.vParent.y + ")" : ""
				});

			// Add circles as node representation
			// fill white, if children are collapsed, else lightblue
			nodesEnter.append('circle')
				.attr('r', 0)
				//.style('fill', d => d._children ? '#fff' : 'lightsteelblue')
				.transition()
				.duration(animDuration)
				.attr('r', nodeRadius)

			let nodesUpdate = nodesEnter.merge(nodes); // This is actually exactly the same as nodeEnter

			// give color
			//nodeUpdate.select('.node circle').style('fill', d => d.data.color);

			// Transition to the proper position for the node
			nodesUpdate.transition()
				.duration(animDuration)
				.attr('transform', d => "translate(" + d.x + "," + d.y + ")")

			// Remove any exiting nodes
			let nodesExit = nodes.exit().transition()
				.duration(animDuration)
				.attr('transform', d => "translate(" + d.parent.x + "," + d.parent.y + ")")
				.remove();

			// On exit reduce the node circles size to 0
			nodesExit.select('.node circle')
				.attr('r', 0);
		}

		_renderLinks() {
			const { animDuration } = this._opts;
			let links = this._linkContainer.selectAll('g.link')
				.data(this._links);

			if (links.length == 0) return this;

			// Create new link divs
			let linksEnter = links.enter().append('g')
				.classed('link', true);

			// Append new link path (parent pos)
			linksEnter.append('path')
				.attr('d', d => this._diagonal(d.source, d.source));
				
			// Animate all paths
			let linksUpdate = linksEnter.merge(links);
			linksUpdate.selectAll('path')
				.transition()
					.duration(animDuration)
					.attr('d', d => this._diagonal(d.source, d.target) )
				//.on("end", () => markAsLatest(latestNodeId)); // mark latest revision

			// Remove exit link divs
			links.exit().selectAll('path')
				.transition()
					.duration(animDuration)
					.attr('d', d => this._diagonal(d.source, d.source))
				.on("end", () => links.exit().remove());
		}

		_reposition() {
			const { margin, nodeRadius, nodeSpacing } = this._opts;
			const container = this._container;
			d3.select('#svg-drawn').attr('transform', function(d) {
				// get the size of the drawing
				let bbox = this.getBBox();
				// move tree to the right of the svg
				let moveX = bbox.width;
				//align it with image nodes
				let moveY = nodeSpacing.y / 2;
				let height = bbox.height + 2*nodeRadius;
				// extra space to actually see the node
				let width = bbox.width + 2*nodeRadius;
				
				// save scroll position (distance from the end of the scrollbar)
				let scrollDistEndX = container.scrollWidth - container.scrollLeft;
				let scrollDistEndY = container.scrollHeight - container.scrollTop;
				
				//resize the surrounding SVG to cover whole drawn space + some margin
				let svg = d3.select(this.parentNode);
				if (width > container.clientWidth)
					svg.attr('width', width);
				else
					svg.attr('width', container.clientWidth);
				
				if (height > container.clientHeight)
					svg.attr('height', (height+moveY))
				else
					svg.attr('height', container.clientHeight-10);// magic -10, otherwies scrollbar is shown
				
				// set scroll bar to the new position, looking at the same position as before the resize
				container.scrollLeft = container.scrollWidth - scrollDistEndX;
				container.scrollTop = container.scrollHeight - scrollDistEndY;
				
				//let string = "translate(" + moveX + "," + moveY + ")";
				let string = "translate(" + (container.scrollWidth - margin.right) + "," + moveY + ")";
				return string;
			});
		}
	}

	d3.myTree = (...args) => new Tree(...args);
}